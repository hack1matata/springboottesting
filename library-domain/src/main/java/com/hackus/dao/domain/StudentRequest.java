package com.hackus.dao.domain;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentRequest {
    String name;
}
