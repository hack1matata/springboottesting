package com.hackus.dao.validation.controller;

import com.hackus.dao.domain.CreateBookRequest;
import com.hackus.dao.domain.StudentRequest;
import com.hackus.dao.domain.ValidationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping()
@Slf4j
public class ValidationController {

    @PostMapping(value = "/validateStudent")
    private ResponseEntity<ValidationResponse> validateStudentRequest(@RequestBody StudentRequest studentRequest){
        System.out.println("The request is not ok;");
        return ResponseEntity.ok()
                .body(ValidationResponse.builder()
                        .valid(true)
                        .build());
    }

    @PostMapping(value = "/validateBook")
    private ResponseEntity<ValidationResponse> validateStudentRequest(@RequestBody CreateBookRequest createBookRequest){
        log.info("The request is ok;");
        return ResponseEntity.ok()
            .body(ValidationResponse.builder()
                .valid(true)
                .build());
    }
}
