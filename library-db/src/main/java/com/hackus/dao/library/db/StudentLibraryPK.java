package com.hackus.dao.library.db;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class StudentLibraryPK implements Serializable {

    @NotNull
    @Column
    private int student_id;

    @NotNull
    @Column
    private String library_id;
}
