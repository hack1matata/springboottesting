package com.hackus.dao.library.db;


import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(name = "book")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BookEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "book_seq_gen")
    @SequenceGenerator(name = "book_seq_gen", sequenceName = "app_book_tbl_id_seq", allocationSize=10)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "student_id", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private StudentEntity studentEntity;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "library_id", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private LibraryEntity libraryEntity;

    @NotNull
    @Column
    private String uuid;

    @NotNull
    @Column(unique = true)
    private String title;

    @NotNull
    @Column
    private String description;
}
