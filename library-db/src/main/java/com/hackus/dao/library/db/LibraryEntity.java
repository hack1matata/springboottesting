package com.hackus.dao.library.db;

import com.sun.istack.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "library")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LibraryEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "library_seq_gen")
    @SequenceGenerator(name = "library_seq_gen", sequenceName = "app_library_tbl_id_seq", allocationSize=10)
    private long id;

    @NotNull
    @Column
    private String uuid;

    @NotNull
    @Column
    private String name;

    @ManyToMany(mappedBy = "libraries", fetch = FetchType.EAGER)
    private Set<StudentEntity> students = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,
        fetch = FetchType.EAGER,
        mappedBy = "libraryEntity")
    Set<BookEntity> books = new HashSet<>();

}
