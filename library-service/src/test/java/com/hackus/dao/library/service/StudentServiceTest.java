package com.hackus.dao.library.service;

import com.hackus.dao.library.DemoApplication;
import com.hackus.dao.library.service.model.StudentDto;
import com.hackus.dao.library.persistence.repository.StudentRepository;
import lombok.val;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static com.hackus.dao.library.utils.Transformers.convertToStudentDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

//@SpringBootTest(classes = DemoApplication.class,
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest(classes = DemoApplication.class)
@EnableAutoConfiguration
@ActiveProfiles("test")
public class StudentServiceTest {

//    @LocalServerPort
//    private int port;

    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;

    @BeforeEach
    public void setup(){
        studentRepository.deleteAll();
    }

    @Test
//    @Transactional
    public void testCreateStudent() {
        StudentDto studentDto = StudentDto.builder()
            .uuid("uuid1")
            .name("Student1")
            .build();

        studentService.createStudent(studentDto);
        assertThat(studentRepository.findAll().size()).isEqualTo(1);
    }

    @Test
//    @Transactional
    public void updateStudent() {
        StudentDto studentDto = StudentDto.builder()
            .uuid("uuid1")
            .name("Vasile")
            .build();

        val createdDto = studentService.createStudent(studentDto);
        assertThat(studentRepository.findAll().size()).isEqualTo(1);

        val updatedStudentDto = createdDto.toBuilder().name("Joric").build();
        studentService.updateStudent(updatedStudentDto);
        assertTrue(studentRepository.findByName("Joric").isPresent());
    }

    @Test
//    @Transactional
    public void deleteStudent() {
        StudentDto studentDto = StudentDto.builder()
            .uuid("uuid1")
            .name("Student1")
            .build();

        studentService.createStudent(studentDto);
        assertThat(studentRepository.findAll().size()).isEqualTo(1);
        studentService.deleteStudent(studentDto);
        assertThat(studentRepository.findAll().size()).isEqualTo(0);
    }
}