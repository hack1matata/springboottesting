package com.hackus.dao.library.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;

@TestConfiguration
public class LocalRibbonClientConfiguration {

//    @Bean
//    public WireMockServer wireMock(){
//        return new WireMockServer(WireMockConfiguration.options().dynamicPort());
//    }

    @Bean
    public ServerList<Server> ribbonServerList(WireMockServer wiremock) {
        wiremock.start();
        return new StaticServerList<>(new Server("localhost", wiremock.port()));
    }
}
