package com.hackus.dao.library.controller;

import com.hackus.dao.library.DemoApplication;
import com.hackus.dao.library.config.LocalRibbonClientConfiguration;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@SpringBootTest(classes = DemoApplication.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {LocalRibbonClientConfiguration.class})
@AutoConfigureWireMock(stubs = {"classpath:/stubs"})
public class StudentControllerFileStubTest {

    //http://wiremock.org/docs/getting-started/
    //https://cloud.spring.io/spring-cloud-contract/reference/html/project-features.html#features-wiremock

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testCreateStudent() throws Exception {
        val response = this.mockMvc.perform(
                MockMvcRequestBuilders.post("/student")
                .content("{ \"name\": \"test123\" }")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType("application/json"));

        System.out.println(response);
    }

}