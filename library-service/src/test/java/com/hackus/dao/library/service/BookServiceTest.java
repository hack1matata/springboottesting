package com.hackus.dao.library.service;

import com.hackus.dao.library.DemoApplication;
import com.hackus.dao.library.persistence.repository.BookRepository;
import com.hackus.dao.library.persistence.repository.StudentRepository;
import com.hackus.dao.library.service.model.BookDto;
import com.hackus.dao.library.service.model.StudentDto;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.val;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static com.hackus.dao.library.utils.Transformers.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

//@SpringBootTest(classes = DemoApplication.class,
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@EnableAutoConfiguration
@ActiveProfiles("test")
@Slf4j
public class BookServiceTest {

//    @LocalServerPort
//    private int port;

    @Autowired
    BookService bookService;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;


    @BeforeEach
    public void setup(){
        bookRepository.deleteAll();
        studentRepository.deleteAll();
    }

    @Test
    public void createBasicBook(){
        BookDto bookDto = BookDto.builder()
            .description("Description1")
            .title("Title1")
            .build();

        val book = bookService.createBook(bookDto);
        assertThat(bookRepository.findAll().size()).isEqualTo(1);
    }

    @Test
//    @Transactional
    public void createBook() {
        StudentDto studentDto = StudentDto.builder()
            .uuid("uuid2")
            .name("Student1")
            .build();

        val studentEntity = studentRepository.save(convertToStudentEntity.apply(studentDto));

        BookDto bookDto = BookDto.builder()
            .description("Description1")
            .title("Title1")
            .student(studentDto)
            .build();

        val book = bookService.createBook(bookDto, studentEntity);
        assertThat(bookRepository.findAll().size()).isEqualTo(1);
    }

    @Test
    @Transactional
    public void findBook() {
        StudentDto studentDto = StudentDto.builder()
            .uuid("uuid2")
            .name("Student1")
            .build();

        val studentEntity = studentRepository.save(convertToStudentEntity.apply(studentDto));

        BookDto bookDto = BookDto.builder()
            .description("Description1")
            .title("Title1")
            .student(studentDto)
            .build();

        bookService.createBook(bookDto, studentEntity);

        val bookFound = bookRepository.findAll().get(0);

        log.info("Student id is: " + bookFound.getStudentEntity().getId());

        assertNotNull(bookFound.getStudentEntity().getName());
    }
}