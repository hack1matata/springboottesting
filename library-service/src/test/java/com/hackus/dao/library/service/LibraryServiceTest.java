package com.hackus.dao.library.service;

import com.hackus.dao.library.DemoApplication;
import com.hackus.dao.library.persistence.repository.BookRepository;
import com.hackus.dao.library.persistence.repository.LibraryRepository;
import com.hackus.dao.library.persistence.repository.StudentLibraryRepository;
import com.hackus.dao.library.persistence.repository.StudentRepository;
import com.hackus.dao.library.service.model.BookDto;
import com.hackus.dao.library.service.model.LibraryDto;
import com.hackus.dao.library.service.model.StudentDto;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import java.util.Optional;
import java.util.Set;

import static com.hackus.dao.library.utils.Transformers.convertToStudentEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Slf4j
public class LibraryServiceTest {

    @Autowired
    BookService bookService;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LibraryRepository libraryRepository;
    @Autowired
    LibraryService libraryService;
    @Autowired
    StudentLibraryRepository studentLibraryRepository;

    @BeforeEach
    public void setup(){
        bookRepository.deleteAll();
        studentRepository.deleteAll();
        libraryRepository.deleteAll();
        studentLibraryRepository.deleteAll();
    }

    @Test
    @Transactional
    public void createLibrary() {
        StudentDto studentDto = StudentDto.builder()
            .uuid("uuid2")
            .name("Student1")
            .build();

        val studentEntity = studentRepository.save(convertToStudentEntity.apply(studentDto));

        BookDto bookDto = BookDto.builder()
            .description("Description1")
            .title("Title1")
            .student(studentDto)
            .build();

        val bookEntity = bookService.createBook(bookDto, studentEntity);
        assertThat(bookRepository.findAll().size()).isEqualTo(1);

        LibraryDto libraryDto = LibraryDto.builder()
            .name("Library1")
            .build();

        val libraryCreated = libraryService.createLibrary(libraryDto);

        val libraryEntity = libraryRepository.findByUuid(libraryCreated.getUuid());

        libraryService.addBookToLibrary(libraryEntity, bookEntity);
        libraryService.addStudentToLibrary(libraryEntity, studentEntity);

        val libraryFetched = libraryRepository.findByUuid(libraryCreated.getUuid());

        assertEquals(1, libraryFetched.getBooks().size());
        assertEquals(1, libraryFetched.getStudents().size());
    }
}