package com.hackus.dao.library.controller;

import com.hackus.dao.domain.StudentRequest;
import com.hackus.dao.domain.StudentResponse;
import com.hackus.dao.library.service.StudentService;
import com.hackus.dao.library.service.ValidationService;
import com.hackus.dao.library.service.model.StudentDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.hackus.dao.library.utils.Transformers.convertStudentRequestToStudentDto;
import static com.hackus.dao.library.utils.Transformers.convertToStudentResponse;

@RestController
@RequiredArgsConstructor
@Slf4j
public class StudentController {

    private final StudentService studentService;
    private final ValidationService validationService;

    @PostMapping(value = "/student")
    public ResponseEntity<StudentResponse> createStudent(@RequestBody StudentRequest request){
        log.info("Received request request: " + request);

        val responseValidation = validationService.validateStudentRequest(request);

        log.info("Client validation result: " + responseValidation.getBody().isValid());

        if(responseValidation.getBody().isValid()) {

            StudentDto studentDto = convertStudentRequestToStudentDto.apply(request);
            val createdStudentDto = studentService.createStudent(studentDto);

            val response = convertToStudentResponse.apply(createdStudentDto);

            return ResponseEntity.ok()
                    .body(response);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
