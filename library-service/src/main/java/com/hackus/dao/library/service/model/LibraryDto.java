package com.hackus.dao.library.service.model;

import lombok.Builder;
import lombok.Value;

import java.awt.print.Book;
import java.util.List;
import java.util.Set;

@Value
@Builder
public class LibraryDto {
    String uuid;
    String name;
    List<StudentDto> students;
    List<BookDto> books;
}
