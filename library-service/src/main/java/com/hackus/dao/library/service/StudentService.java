package com.hackus.dao.library.service;

import com.hackus.dao.library.persistence.repository.StudentRepository;
import com.hackus.dao.library.service.model.StudentDto;
import lombok.Data;
import lombok.val;
import org.springframework.stereotype.Service;

import static com.hackus.dao.library.utils.Transformers.convertToStudentDto;
import static com.hackus.dao.library.utils.Transformers.convertToStudentEntity;

@Data
@Service
public class StudentService {
    private final StudentRepository studentRepository;

    public StudentDto createStudent(StudentDto student){
        val studentEntity = convertToStudentEntity.apply(student);
        return convertToStudentDto.apply(studentRepository.save(studentEntity));
    }

    public void updateStudent(StudentDto student){
        val persistentEntity = studentRepository.findByUuid(student.getUuid());
        persistentEntity.get().setName(student.getName());
        studentRepository.save(persistentEntity.get());
    }

    public void deleteStudent(StudentDto student){
        val studentEntity = studentRepository.findByUuid(student.getUuid());
        studentRepository.delete(studentEntity.get());
    }
}
