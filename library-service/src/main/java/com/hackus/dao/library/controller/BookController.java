package com.hackus.dao.library.controller;

import com.hackus.dao.domain.CreateBookRequest;
import com.hackus.dao.domain.CreateBookResponse;
import com.hackus.dao.library.service.BookService;
import com.hackus.dao.library.service.ValidationService;
import com.hackus.dao.library.service.model.BookDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.hackus.dao.library.utils.Transformers.convertDtoToBookResponse;
import static com.hackus.dao.library.utils.Transformers.convertToBookDto;

@RestController
@RequiredArgsConstructor
@Slf4j
public class BookController {

    private final BookService bookService;
    private final ValidationService validationService;

    @PostMapping(value = "/book")
    public ResponseEntity<CreateBookResponse> createBook(@RequestBody CreateBookRequest request){
        log.info("Received request: " + request);

        val responseValidation = validationService.validateBookRequest(request);

        log.info("Client validation result: " + responseValidation.getBody().isValid());

        if(responseValidation.getBody().isValid()) {

            BookDto book = convertToBookDto.apply(request);
            val createdBookDto = bookService.createBook(book);

            val response = convertDtoToBookResponse.apply(createdBookDto);

            return ResponseEntity.ok()
                    .body(response);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
