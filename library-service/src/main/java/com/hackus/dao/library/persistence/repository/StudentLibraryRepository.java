package com.hackus.dao.library.persistence.repository;

import com.hackus.dao.library.db.StudentEntity;
import com.hackus.dao.library.db.StudentLibraryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentLibraryRepository extends JpaRepository<StudentLibraryEntity, Long> {
}
