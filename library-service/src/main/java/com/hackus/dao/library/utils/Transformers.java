package com.hackus.dao.library.utils;

import com.hackus.dao.domain.CreateBookRequest;
import com.hackus.dao.domain.CreateBookResponse;
import com.hackus.dao.domain.CreateLibraryRequest;
import com.hackus.dao.domain.CreateLibraryResponse;
import com.hackus.dao.domain.StudentRequest;
import com.hackus.dao.domain.StudentResponse;
import com.hackus.dao.library.db.LibraryEntity;
import com.hackus.dao.library.service.model.BookDto;
import com.hackus.dao.library.service.model.LibraryDto;
import com.hackus.dao.library.service.model.StudentDto;
import com.hackus.dao.library.db.BookEntity;
import com.hackus.dao.library.db.StudentEntity;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Transformers {

    public static Function<BookDto, BookEntity> convertToBookEntity =
        bookDto -> BookEntity.builder()
            .uuid(UUID.randomUUID().toString())
            .title(bookDto.getTitle())
            .description(bookDto.getDescription())
            .build();

    public static Function<List<BookDto>, Set<BookEntity>> convertToBookEntityList =
        bookDtos -> bookDtos.stream()
            .map(convertToBookEntity)
            .collect(Collectors.toSet());

    public static Function<CreateBookRequest, BookDto> convertToBookDto =
        request -> BookDto.builder()
            .title(request.getTitle())
            .description(request.getDescription())
            .build();

    public static Function<StudentDto, StudentEntity> convertToStudentEntity =
        studentDto -> StudentEntity.builder()
            .uuid(studentDto.getUuid())
            .name(studentDto.getName())
            .build();

    public static Function<List<StudentDto>, Set<StudentEntity>> convertToStudentEntityList =
        studentDtos -> studentDtos.stream()
            .map(convertToStudentEntity)
            .collect(Collectors.toSet());

    public static Function<StudentEntity, StudentDto> convertToStudentDto =
            studentEntity -> StudentDto.builder()
                    .uuid(studentEntity.getUuid())
                    .name(studentEntity.getName())
//            .books(convertToBookEntityList.apply(studentDto.getBooks()))
                    .build();

    public static Function<Set<StudentEntity>, List<StudentDto>> convertSetEntityToStudentDtoList =
        studentEntities -> studentEntities.stream()
            .map(convertToStudentDto)
            .collect(Collectors.toList());


    public static Function<StudentRequest, StudentDto> convertStudentRequestToStudentDto =
        request -> StudentDto.builder()
            .uuid(UUID.randomUUID().toString())
            .name(request.getName())
            .build();

    public static Function<StudentDto, StudentResponse> convertToStudentResponse =
        studentDto -> StudentResponse.builder()
            .name(studentDto.getName())
            .uuid(studentDto.getUuid())
            .build();

    public static Function<BookEntity, BookDto> convertEntityToBookDto =
        entity -> BookDto.builder()
            .uuid(entity.getUuid())
            .title(entity.getTitle())
            .description(entity.getDescription())
            .student(Optional.ofNullable(entity.getStudentEntity())
                .map(student -> convertToStudentDto.apply(entity.getStudentEntity()))
                .orElse(null))
            .build();

    public static Function<Set<BookEntity>, List<BookDto>> convertSetEntityToBookDtoList =
        bookEntities -> bookEntities.stream()
            .map(convertEntityToBookDto)
            .collect(Collectors.toList());

    public static Function<BookDto, CreateBookResponse> convertDtoToBookResponse =
        dto -> CreateBookResponse.builder()
            .uuid(dto.getUuid())
            .title(dto.getTitle())
            .description(dto.getDescription())
            .studentUuid(Optional.ofNullable(dto.getStudent()).map(student -> student.getUuid()).orElse(null))
            .build();

    public static Function<CreateLibraryRequest, LibraryDto> convertToLibraryDto =
        request -> LibraryDto.builder()
            .name(request.getName())
            .build();

    public static Function<LibraryDto, LibraryEntity> convertToLibraryEntity =
        libraryDto -> LibraryEntity.builder()
            .uuid(Optional.ofNullable(libraryDto.getUuid()).map(Function.identity()).orElse(null))
            .books(Optional.ofNullable(libraryDto.getBooks()).map(books -> convertToBookEntityList.apply(books)).orElse(null))
            .name(libraryDto.getName())
            .students(Optional.ofNullable(libraryDto.getStudents()).map(students -> convertToStudentEntityList.apply(students)).orElse(null))
            .build();

    public static Function<LibraryEntity, LibraryDto> convertEntityToLibraryDto =
        libraryEntity -> LibraryDto.builder()
            .uuid(libraryEntity.getUuid())
            .books(Optional.ofNullable(libraryEntity.getBooks()).map(books -> convertSetEntityToBookDtoList.apply(books)).orElse(null))
            .name(libraryEntity.getName())
            .students(Optional.ofNullable(libraryEntity.getStudents()).map(students -> convertSetEntityToStudentDtoList.apply(students)).orElse(null))
            .build();

    public static Function<LibraryDto, CreateLibraryResponse> convertToCreateLibraryResponse =
        libraryDto -> CreateLibraryResponse.builder()
            .uuid(libraryDto.getUuid())
            .build();
}
