package com.hackus.dao.library.service;

import com.hackus.dao.library.db.BookEntity;
import com.hackus.dao.library.db.LibraryEntity;
import com.hackus.dao.library.db.StudentEntity;
import com.hackus.dao.library.persistence.repository.BookRepository;
import com.hackus.dao.library.persistence.repository.LibraryRepository;
import com.hackus.dao.library.persistence.repository.StudentRepository;
import com.hackus.dao.library.service.model.BookDto;
import com.hackus.dao.library.service.model.LibraryDto;
import com.hackus.dao.library.utils.Transformers;
import lombok.Data;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.hackus.dao.library.utils.Transformers.convertEntityToBookDto;
import static com.hackus.dao.library.utils.Transformers.convertEntityToLibraryDto;

@Data
@Service
public class LibraryService {
    private final LibraryRepository libraryRepository;

    public LibraryDto createLibrary(LibraryDto libraryDto){
        val libraryEntity = Transformers.convertToLibraryEntity.apply(libraryDto);
        return convertEntityToLibraryDto.apply(libraryRepository.save(libraryEntity));
    }

    @Transactional
    public LibraryEntity addBookToLibrary(LibraryEntity libraryEntity, BookEntity bookEntity){

        val set = new HashSet<BookEntity>();
        set.add(bookEntity);

        Optional.ofNullable(libraryEntity.getBooks())
            .ifPresentOrElse(books -> libraryEntity.getBooks().add(bookEntity),
                () -> libraryEntity.setBooks(set));

        return libraryRepository.save(libraryEntity);
    }

    @Transactional
    public LibraryEntity addStudentToLibrary(LibraryEntity libraryEntity, StudentEntity studentEntity){
        val set = new HashSet<StudentEntity>();
        set.add(studentEntity);

        Optional.ofNullable(libraryEntity.getStudents())
            .ifPresentOrElse(books -> libraryEntity.getStudents().add(studentEntity),
                () -> libraryEntity.setStudents(set));

        return libraryRepository.save(libraryEntity);
    }

    @Transactional
    public void deleteLibrary(LibraryEntity libraryEntity){
        libraryRepository.delete(libraryEntity);
    }
}
