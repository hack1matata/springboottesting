package com.hackus.dao.library.persistence.repository;

import com.hackus.dao.library.db.BookEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {

    BookEntity findByStudentEntityId(Long studentId);
    Optional<BookEntity> findByIdAndStudentEntityId(Long id, Long postId);
//
//    @Override
//    List<BookEntity> findAll();
}
