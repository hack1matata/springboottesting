package com.hackus.dao.library.persistence.repository;

import com.hackus.dao.library.db.BookEntity;
import com.hackus.dao.library.db.LibraryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface LibraryRepository extends JpaRepository<LibraryEntity, Long> {

    LibraryEntity findByUuid(String uuid);
}
