package com.hackus.dao.library.service;


import com.hackus.dao.domain.CreateBookRequest;
import com.hackus.dao.domain.StudentRequest;
import com.hackus.dao.domain.ValidationResponse;
import com.hackus.dao.library.config.CustomFeignConfig;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value="validation-service", configuration = CustomFeignConfig.class)
@RibbonClient(name = "validation-service")
//@FeignClient(name="validation-service")
public interface ValidationService {

    @PostMapping(value = "/validateStudent")
    ResponseEntity<ValidationResponse> validateStudentRequest(@RequestBody StudentRequest studentRequest);

    @PostMapping(value = "/validateBook")
    ResponseEntity<ValidationResponse> validateBookRequest(@RequestBody CreateBookRequest studentRequest);
}
