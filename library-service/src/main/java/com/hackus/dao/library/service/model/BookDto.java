package com.hackus.dao.library.service.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class BookDto {
    String uuid;
    String title;
    String description;
    StudentDto student;
}
