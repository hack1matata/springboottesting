package com.hackus.dao.library.service;

import com.hackus.dao.library.db.BookEntity;
import com.hackus.dao.library.db.StudentEntity;
import com.hackus.dao.library.persistence.repository.StudentRepository;
import com.hackus.dao.library.utils.Transformers;
import com.hackus.dao.library.persistence.repository.BookRepository;
import com.hackus.dao.library.service.model.BookDto;

import lombok.Data;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.hackus.dao.library.utils.Transformers.convertEntityToBookDto;

@Data
@Service
public class BookService {
    private final BookRepository bookRepository;
    private final StudentRepository studentRepository;

    public BookDto createBook(BookDto book){
        val bookEntity = Transformers.convertToBookEntity.apply(book);
        return convertEntityToBookDto.apply(bookRepository.save(bookEntity));
    }

    public BookEntity createBook(BookDto book, StudentEntity studentEntity){
        val bookEntity = Transformers.convertToBookEntity.apply(book);
//        val student = studentRepository.findByUuid(book.getStudent().getUuid());
        bookEntity.setStudentEntity(studentEntity);
        return bookRepository.save(bookEntity);
    }

    @Transactional
    public void updateBook(BookDto book){
        val bookEntity = Transformers.convertToBookEntity.apply(book);
        bookRepository.save(bookEntity);
    }

    @Transactional
    public void deleteBook(BookDto book){
        val bookEntity = Transformers.convertToBookEntity.apply(book);
        bookRepository.delete(bookEntity);
    }
}
