package com.hackus.dao.library.config;

import feign.Contract;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomFeignConfig {
    @Bean
//    public Contract feignContract() {
//        return new feign.Contract.Default();
//    }
    public Contract feignContract() {
        return new SpringMvcContract();
    }

//    @Bean
//    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
//        return new BasicAuthRequestInterceptor("user", "password");
//    }

//    @Bean
//    public OkHttpClient client() {
//        return new OkHttpClient();
//    }
}
