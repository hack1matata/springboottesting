package com.hackus.dao.library.controller;

import com.hackus.dao.domain.CreateBookRequest;
import com.hackus.dao.domain.CreateBookResponse;
import com.hackus.dao.domain.CreateLibraryRequest;
import com.hackus.dao.domain.CreateLibraryResponse;
import com.hackus.dao.library.service.BookService;
import com.hackus.dao.library.service.LibraryService;
import com.hackus.dao.library.service.ValidationService;
import com.hackus.dao.library.service.model.BookDto;
import com.hackus.dao.library.service.model.LibraryDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.hackus.dao.library.utils.Transformers.convertDtoToBookResponse;
import static com.hackus.dao.library.utils.Transformers.convertToBookDto;
import static com.hackus.dao.library.utils.Transformers.convertToCreateLibraryResponse;
import static com.hackus.dao.library.utils.Transformers.convertToLibraryDto;

@RestController
@RequiredArgsConstructor
@Slf4j
public class LibraryController {

    private final LibraryService libraryService;

    @PostMapping(value = "/library")
    public ResponseEntity<CreateLibraryResponse> createLibrary(@RequestBody CreateLibraryRequest request){
        log.info("Received request: " + request);

        LibraryDto library = convertToLibraryDto.apply(request);
        val createdLibraryDto = libraryService.createLibrary(library);

        val response = convertToCreateLibraryResponse.apply(createdLibraryDto);

        return ResponseEntity.ok()
                .body(response);
    }
}
