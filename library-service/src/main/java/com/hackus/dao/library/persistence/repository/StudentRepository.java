package com.hackus.dao.library.persistence.repository;

import com.hackus.dao.library.db.StudentEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
    Optional<StudentEntity> findByName(String name);
    Optional<StudentEntity> findByUuid(String uuid);
}
