package com.hackus.dao.library.service.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class StudentDto {
    String uuid;
    String name;
}
